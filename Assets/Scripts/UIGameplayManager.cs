﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIGameplayManager : MonoBehaviour
{
    [SerializeField] Text score;
    [SerializeField] Text time;
    [SerializeField] Text rightPanelInfo;
    [SerializeField] Slider fuelSlider;

    [SerializeField] Text landingResult;
    [SerializeField] Text landingScore;

    [SerializeField] GameObject tryAgainButton;
    [SerializeField] GameObject NextLevelButton;
    [SerializeField] GameObject MenuButton;

    [SerializeField] GameObject pausePanel;
    [SerializeField] GameObject generalPanel;
    [SerializeField] GameObject landingPanel;

    Player player;

    public void ResetUI()
    {
        TurnGeneralPanels(true);
        TurnLandingPanel(false);
        TurnPausePanels(false);
    }

    public void TurnGeneralPanels(bool active)
    {
        generalPanel.SetActive(active);
    }

    public void TurnPausePanels(bool active)
    {
        pausePanel.SetActive(active);
    }

    public void TurnLandingPanel(bool active)
    {
        landingPanel.SetActive(active);
    }

    public void GoToMenu()
    {
        GameManager.Get().GoToScene("Menu");
    }

    string FormatTime(float fTime)
    {
        TimeSpan t = TimeSpan.FromSeconds(fTime);

        return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
    }

    void UpdateTime(float actualTime)
    {
        time.text = "\nTime: " + FormatTime(actualTime);
    }

    void UpdateScore(int scoreAmount)
    {
        score.text = "Score: " + scoreAmount;
    }

    void UpdateRightPanel(Vector2 speed, float YPos)
    {
        speed *= 10f;

        rightPanelInfo.text = "Altitude: " + (int)YPos + "\n   speed: " + (int)speed.x + "\n   speed: " + (int)speed.y;
    }

    void UpdateFuel(float fuelAmount)
    {
        fuelSlider.value = fuelAmount / 100f;
    }

    void SetLandingScreen(bool playerDead, int score)
    {
        TurnLandingPanel(true);
        landingResult.text = !playerDead ? "Succesful landing" : "What was that?";
        landingScore.text = "Score +" + score.ToString();

        tryAgainButton.SetActive(playerDead);
        NextLevelButton.SetActive(!playerDead);
        MenuButton.SetActive(true);
    }

    void Start()
    {
        player = GameplayManager.GetInstance().GetPlayer();

        player.onChangedDirAndPos += UpdateRightPanel;
        player.onChangedFuelValue += UpdateFuel;
        player.onChangedScore += UpdateScore;

        GameplayManager.GetInstance().onTimeChanged += UpdateTime;
        GameplayManager.GetInstance().onPlayerLanded += SetLandingScreen;
        GameplayManager.GetInstance().onRestartLevel += ResetUI;
    }

    void OnDestroy()
    {
        player.onChangedDirAndPos -= UpdateRightPanel;
        player.onChangedFuelValue -= UpdateFuel;
        player.onChangedScore -= UpdateScore;

        GameplayManager.GetInstance().onTimeChanged -= UpdateTime;
        GameplayManager.GetInstance().onPlayerLanded -= SetLandingScreen;
        GameplayManager.GetInstance().onRestartLevel -= ResetUI;
    }
}
