﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinSpace : MonoBehaviour
{
    public Action<int> onTriggerActivated;

    void OnTriggerEnter2D(Collider2D collision)
    {
        onTriggerActivated?.Invoke((int)transform.lossyScale.x);
    }
}
