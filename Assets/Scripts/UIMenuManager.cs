﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMenuManager : MonoBehaviour
{
    #region Singleton
    private static UIMenuManager instance;

    public static UIMenuManager GetInstace()
    {
        return instance;
    }

    void Awake()
    {
        instance = this;
    }
    #endregion

    [SerializeField] GameObject menu;
    [SerializeField] GameObject title;
    [SerializeField] GameObject credits;
    [SerializeField] GameObject splashScreen;
    [SerializeField] BackgroundController background;

    Animator anim;

    public void GoToGame()
    {
        GameManager.Get().GoToScene("Game");
        LoaderManager.Get().LoadScene("Game");
        UILevelLoader.Get().SetVisible(true);
    }    

    public void ExitGame()
    {
        GameManager.Get().ExitGame();
    }

    public void SetMenuActive()
    {
        title.SetActive(true);
        menu.SetActive(true);
        credits.SetActive(false);
    }

    public void SetCreditsActive()
    {
        title.SetActive(false);
        menu.SetActive(false);
        credits.SetActive(true);
    }

    public void PowerOffSplashScreen()
    {
        splashScreen.SetActive(false);
        anim.enabled = false;
    }

    void Start()
    {
        Time.timeScale = 1;
        anim = GetComponent<Animator>();
    }

    void Update()
    {   
        Vector2 pos = Input.mousePosition;
        background.Move(pos, new Vector2(0,0));
    }    
}
