﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    #region Singleton
    private static GameplayManager instance;

    public static GameplayManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }

        instance = this;
    }
    #endregion

    [SerializeField] Camera mainCamera;
    [SerializeField] Player player;
    [SerializeField] TerrainGenerator terrainGenerator;
    [SerializeField] BackgroundController background;

    public const float initialFuel = 100f; 

    public bool playerdead = false;
    bool cameraZoomed = false;

    bool gamePaused = false;

    float cameraSize = 20f;
    float cameraSideSize = 35.5f;

    public Action<bool, int> onPlayerLanded;
    public Action<float> onTimeChanged;
    public Action onRestartLevel;

    TerrainCube[] terrain;
    [SerializeField] List<GameObject> winSpace;

    Vector2 screenCenter;

    float time = 0;

    public Player GetPlayer()
    {
        return player;
    }

    public void PauseGame(bool pause)
    {
        //gamePaused = pause;
        Time.timeScale = pause ? 0 : 1;
        player.SetLocked(pause);
        //player.enabled = !pause;
    }

    void SetPlayerCollision(Vector2 playerVelocity, float playerRotation)
    {
        float maxVelocity = 10f;
        float maxRotation = 10f;
        playerVelocity.x = Mathf.Abs(playerVelocity.x);
        playerVelocity.y = Mathf.Abs(playerVelocity.y);

        if (Mathf.Abs(playerVelocity.x) > maxVelocity || Mathf.Abs(playerVelocity.y) > maxVelocity || Mathf.Abs(playerRotation) > maxRotation)
        {
            playerdead = true;
            onPlayerLanded?.Invoke(playerdead, 0);
            PauseGame(true);
        }
    }

    IEnumerator TimeWin(int score)
    {
        float secondsToWin = 1f;

        yield return new WaitForSeconds(secondsToWin);

        onPlayerLanded?.Invoke(playerdead, score);
        player.AddScore(score);
        PauseGame(true);
    }

    void Win(int score)
    {
        int scoreMultiplier = 100;
        StopCoroutine(TimeWin(scoreMultiplier / score));
        StartCoroutine(TimeWin(scoreMultiplier / score));
    }

    void SetCameraInsideOfBackground(Vector3 actualCameraPosition)
    {
        float minXPos = screenCenter.x - cameraSideSize + cameraSideSize / 2f;
        float maxXPos = screenCenter.x + cameraSideSize - cameraSideSize / 2f;
        float maxYPos = screenCenter.y + cameraSize - cameraSize / 2f;

        if (actualCameraPosition.x < minXPos)
        {
            actualCameraPosition.x = minXPos;
        }
        else if (actualCameraPosition.x > maxXPos)
        {
            actualCameraPosition.x = maxXPos;
        }

        if (actualCameraPosition.y > maxYPos)
        {
            actualCameraPosition.y = maxYPos;
        }

        mainCamera.transform.position = actualCameraPosition;
    }

    void ChangePlayerSize(bool bigger)
    {
        //!bigger == normal size
        Vector2 size = bigger ? new Vector2(2, 2) : new Vector2(1, 1);

        player.transform.localScale = size;
    }

    void ZoomInCamera(Vector3 position)
    {
        if (cameraZoomed) return;

        position += Vector3.back * cameraSize / 2f;

        SetCameraInsideOfBackground(position);

        mainCamera.orthographicSize = cameraSize / 2f;

        cameraZoomed = true;

        ChangePlayerSize(false);
    }

    void UpdateZoomedCamera()
    {
        if (!cameraZoomed) return;

        Vector3 camPos = mainCamera.transform.position;
        Vector3 playerPos = player.transform.position;

        float diffInX = playerPos.x - camPos.x;
        float diffInY = playerPos.y - camPos.y;

        if (Mathf.Abs(diffInY) > cameraSize / 4f)
        {
            camPos.y += diffInY - cameraSize / 4f;
        }

        if (Mathf.Abs(diffInX) > cameraSideSize / 4f)
        {
            camPos.x += diffInX - cameraSideSize / 4f;
        }

        SetCameraInsideOfBackground(camPos);
    }

    void ResetCamera()
    {
        Vector3 pos;

        pos.x = screenCenter.x;
        pos.y = screenCenter.y;
        pos.z = -TerrainGenerator.length;

        mainCamera.transform.position = pos;
        mainCamera.orthographicSize = cameraSize;

        cameraZoomed = false;

        ChangePlayerSize(true);
    }

    void OnPlayerMoving(Vector3 position, bool closeToGround, bool farFromGround)
    {
        background.Move(position, screenCenter);

        if (closeToGround) 
        { 
            ZoomInCamera(position); 
        }

        UpdateZoomedCamera();

        if (farFromGround)
        {
            ResetCamera();
        }
    }

    void GetLevelInformation()
    {
        terrain = new TerrainCube[TerrainGenerator.length];

        for (int i = 0; i < TerrainGenerator.length; i++)
        {
            terrain[i] = terrainGenerator.GetTerrain(i);
        }

        winSpace = terrainGenerator.GetWinSpaces();

        for (int i = 0; i < winSpace.Count; i++)
        {
            winSpace[i].GetComponent<WinSpace>().onTriggerActivated += Win;
        }        

        screenCenter.x = TerrainGenerator.length / 2f;
        screenCenter.y = TerrainGenerator.maxSize / 2f + terrainGenerator.GetMinSizeExistent() / 2f;

        ResetCamera();

        background.Move(mainCamera.transform.position, mainCamera.transform.position);
    }

    public void RestartGame()
    {
        PauseGame(false);
        time = 0f;

        terrainGenerator.ResetValues();
        terrainGenerator.SetTerrain();

        player.RestartValues(initialFuel);
        onRestartLevel?.Invoke();

        //winSpace.Clear();
        //winSpace = null;

        GetLevelInformation();
        playerdead = false;
    }

    public void GoToNextLevel()
    {
        PauseGame(false);
        terrainGenerator.ResetValues();
        terrainGenerator.SetTerrain();

        player.SetToInitialPosition();
        onRestartLevel?.Invoke();

        GetLevelInformation();
        playerdead = false;
    }

    void Start()
    {
        GetLevelInformation();

        player.onCollision += SetPlayerCollision;
        player.onMoving += OnPlayerMoving;
        player.SetFuel(initialFuel);

        Debug.Log(name);
    }

    void Update()
    {
        if (gamePaused) 
        {
            Vector2 pos = Input.mousePosition / 2f;
            background.Move(pos, screenCenter);
            return;
        }

        time += Time.deltaTime;

        onTimeChanged?.Invoke(time);
    }

    void OnDestroy()
    {
        player.onCollision -= SetPlayerCollision;
        player.onMoving -= OnPlayerMoving;
    }
}
