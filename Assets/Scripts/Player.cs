﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] GameObject[] rockets;
    [SerializeField] LayerMask collisionLayer;

    public Action<Vector2, float> onCollision;
    public Action<Vector3, bool, bool> onMoving;
    public Action<Vector2, float> onChangedDirAndPos;
    public Action<float> onChangedFuelValue;
    public Action<int> onChangedScore;

    Vector2 initialPosition;

    [SerializeField] float speed = 2f;
    [SerializeField] float rotationSpeed = 0.2f;
    [SerializeField] float gravity;
    [SerializeField] float fuel = 0f;

    int score = 0;

    float minDistanceToGround = 4f;
    float maxDistanceToGround = 20f;

    [SerializeField] Vector2 direction = Vector2.zero;
    Vector2 input = Vector2.zero;

    Rigidbody2D body;

    bool locked = false;

    public void ResetBody()
    {
        body.Sleep();
        body.WakeUp();
    }

    public void RestartValues(float initialFuelAmount)
    {
        SetFuel(initialFuelAmount);
        SetToInitialPosition();

        score = 0;
        onChangedScore?.Invoke(score);

        direction = Vector2.zero;
        input = Vector2.zero;
    }

    public void SetLocked(bool lockedState)
    {
        locked = lockedState;
    }

    public void SetFuel(float fuelAmount)
    {
        fuel = fuelAmount;
        onChangedFuelValue?.Invoke(fuel);
    }

    public float GetFuel()
    {
        return fuel;
    }

    public void AddScore(int addedScore)
    {
        score += addedScore;

        onChangedScore?.Invoke(score);
    }

    public int GetScore()
    {
        return score;
    }

    public void SetToInitialPosition()
    {
        transform.position = initialPosition;
        transform.eulerAngles = new Vector3(0, 0, -90);
        ResetBody();
        locked = false;
    }

    bool IsMoving()
    {
        if (locked) return false;

        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");

        return Mathf.Abs(input.x) > 0f || input.y > 0f; 
    }

    /*void ApplyGravity()
    {
        float maxGravity = 10f;
        if (input.y < Mathf.Epsilon)
        {
            if (gravity <= maxGravity)
                gravity += 0.02f;
        }
        else
        {
            if (gravity > initialGravity)
                gravity -= 0.01f;
        }

        transform.position -= Vector3.up * gravity * Time.deltaTime;
    }*/

    void ConsumeFuel()
    {
        float fuelMultiplier = 0.1f;
        fuel -= direction.y * Time.deltaTime * fuelMultiplier;

        onChangedFuelValue?.Invoke(fuel);
    }

    void SetRockets(bool active)
    {
        for (int i = 0; i < rockets.Length; i++)
        {
            ParticleSystem ps = rockets[i].GetComponentInChildren<ParticleSystem>();

            if (active)
            {
                if (!ps.isPlaying)
                    ps.Play();
            }
            else
                ps.Stop();
        }
    }

    void SetMovement()
    {
        direction = input;

        transform.Rotate(-direction.x * Vector3.forward * rotationSpeed);

        body.AddForce(transform.up * speed * direction.y, ForceMode2D.Force);
    }

    bool CheckCloseToGround(float distance)
    {
        Vector2 pos = transform.position;
        Vector2 halfSize = transform.localScale / 2f;

        Vector2 leftDown = Vector3.Normalize(Vector3.left + Vector3.down);
        Vector2 rightDown = Vector3.Normalize(Vector3.right + Vector3.down);

        return Physics2D.Raycast(pos + (halfSize.y * Vector2.down),  Vector2.down, distance, collisionLayer) ||
               Physics2D.Raycast(pos + (halfSize.x * Vector2.left),  Vector2.left, distance, collisionLayer) ||
               Physics2D.Raycast(pos + (halfSize.x * Vector2.right), Vector2.right, distance, collisionLayer) ||
               Physics2D.Raycast(pos + (halfSize.x * Vector2.left)  + (halfSize.y * Vector2.down), leftDown, distance, collisionLayer) ||
               Physics2D.Raycast(pos + (halfSize.x * Vector2.right) + (halfSize.y * Vector2.down), rightDown, distance, collisionLayer);
    }

    bool CheckBodyHasVelocity()
    {
        return Mathf.Abs(body.velocity.x) + Mathf.Abs(body.velocity.y) > 0f;
    }

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();

        gravity = body.gravityScale;
        initialPosition = transform.position;
    }

    void Update()
    {
        if (IsMoving())
        {
            SetMovement();
            ConsumeFuel();
        }

        SetRockets(input.y > 0f);

        if (CheckBodyHasVelocity())
        { 
            onMoving?.Invoke(transform.position, CheckCloseToGround(minDistanceToGround), !CheckCloseToGround(maxDistanceToGround));
            onChangedDirAndPos?.Invoke(body.velocity, transform.position.y);
        }

        body.gravityScale = gravity; // to modify from inspector
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collisionLayer == (collisionLayer | (1 << collision.transform.gameObject.layer)))
        {
            onCollision?.Invoke(body.velocity, transform.eulerAngles.z);
            locked = true;
        }
    }
}
