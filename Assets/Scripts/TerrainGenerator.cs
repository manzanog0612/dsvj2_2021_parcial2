﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TerrainCube
{
    public GameObject go;
    public int height;
    public bool hasWiningSpace;
}

public class TerrainGenerator : MonoBehaviour
{
    [SerializeField] GameObject winSpacePrefab;
    [SerializeField] GameObject floorPrefab;
    [SerializeField] GameObject edgeColliders;

    public const int maxSize = 45;       //max length of a cube in the height map
    public const int scanRadius = 1;
    public const int length = 72;        //amount of cubes in height map

    int minSizeExistent = maxSize;
    
    TerrainCube[] terrain = new TerrainCube[length];

    [SerializeField] List<GameObject> winSpaces;

    public void ResetValues()
    {
        for (int i = 0; i < terrain.Length; i++)
        {
            Destroy(terrain[i].go);
            terrain[i].go = null;
            terrain[i].hasWiningSpace = false;
            terrain[i].height = 0;
        }

        for (int i = 0; i < winSpaces.Count; i++)
        {
            Destroy(winSpaces[i]);
        }

        winSpaces.Clear();

        minSizeExistent = maxSize;
    }

    public void SetTerrain()
    {
        for (int i = 0; i < length; i++)
        {
            terrain[i] = new TerrainCube();
            terrain[i].height = UnityEngine.Random.Range(1, maxSize);
            GameObject gameObject = Instantiate(floorPrefab);
            terrain[i].go = gameObject;

            TransformGO(i);
        }

        Smooth(); Smooth();

        GenerateWinningSpaces();

        SetMinSizeExistent();

        PositionEdgeColliders(new Vector3(length / 2f, maxSize / 2f + minSizeExistent / 2f, -length));
    }

    public int GetMinSizeExistent()
    {
        return minSizeExistent;
    }

    void SetMinSizeExistent()
    {
        for (int i = 0; i < terrain.Length; i++)
        {
            if (terrain[i].height < minSizeExistent)
            { 
                minSizeExistent = terrain[i].height; 
            }
        }
    }

    public List<GameObject> GetWinSpaces()
    {
        return winSpaces;
    }

    public TerrainCube GetTerrain(int index)
    {
        return terrain[index];
    }

    void TransformGO(int i)
    {
        GameObject gameObject = terrain[i].go;
        gameObject.transform.localScale = new Vector3(1, terrain[i].height, 1);
        gameObject.transform.localPosition = new Vector3(i, terrain[i].height / 2f, 0);
        gameObject.name = "column " + i + " (height=" + terrain[i].height + ")";
        gameObject.transform.parent = transform;
    }

    void Smooth()
    {
        for (int i = 0; i < terrain.Length; i++)
        {
            int heightSum = 0;
            int heightCount = 0;

            for (int n = i - scanRadius; n < i + scanRadius + 1; n++)
            {
                if (n >= 0 && n < terrain.Length)
                {
                    int heightOfNeighbour = terrain[n].height;

                    heightSum += heightOfNeighbour;
                    heightCount++;
                }
            }

            int heightAverage = heightSum / heightCount;
            terrain[i].height = heightAverage;

            TransformGO(i);
        }

        //clip first 8 beacuse of player initial position
        int firstCubesMaxSize = 35;

        for (int i = 0; i < 8; i++)
        {
            if (terrain[i].height > firstCubesMaxSize)
            {
                terrain[i].height = firstCubesMaxSize;
                TransformGO(i);
            }            
        }
    }

    void GenerateWinningSpaces()
    {
        int amount = UnityEngine.Random.Range(3, 6); // from 3 to 5

        for (int i = 0; i < amount; i++)
        {
            int lenghtOfWinSpace = UnityEngine.Random.Range(1, 4); // from 1 to 3
            int randomStart = UnityEngine.Random.Range(1, 21);
            bool lookLeftToRight = i % 2 == 0;

            List<TerrainCube> winSpacesCubes = FindConsecutiveSpaces(lenghtOfWinSpace, lookLeftToRight ? randomStart : terrain.Length - randomStart, lookLeftToRight);

            if (winSpacesCubes.Count == 0)
            {
                winSpacesCubes = FindConsecutiveSpaces(1, lookLeftToRight ? randomStart : terrain.Length - randomStart, lookLeftToRight);
            }

            Vector3 position = Vector3.zero;

            for (int j = 0; j < winSpacesCubes.Count; j++)
            {
                position += winSpacesCubes[j].go.transform.position;
                winSpacesCubes[j].hasWiningSpace = true;
            }

            position /= winSpacesCubes.Count;

            position += Vector3.up * (winSpacesCubes[0].height / 2f + winSpacePrefab.transform.lossyScale.y / 2f);

            GameObject winSpace = Instantiate(winSpacePrefab, position, Quaternion.identity, transform);

            winSpace.transform.localScale = winSpacePrefab.transform.localScale + Vector3.right * (winSpacesCubes.Count - 1);

            winSpaces.Add(winSpace);

            
            

            /*for (int j = 0; j < winSpacesCubes.Count; j++)
            {
                TerrainCube cube = winSpacesCubes[j];

                Vector3 position = new Vector3(cube.go.transform.position.x, cube.go.transform.position.y + cube.height / 2f + winSpacePrefab.transform.lossyScale.y / 2f, 0f);

                GameObject winSpace = Instantiate(winSpacePrefab, position, Quaternion.identity, transform);

                winSpaces.Add(winSpace);

                winSpacesCubes[j].hasWiningSpace = true;
            }*/
        }
    }

    void ProcessTerrainCubes(List<TerrainCube> list, bool nextToEachother, TerrainCube auxCube, TerrainCube actualCube)
    {
        if (auxCube.height == actualCube.height && nextToEachother && !actualCube.hasWiningSpace && !auxCube.hasWiningSpace)
        {
            if (!list.Contains(auxCube))
                list.Add(auxCube);

            list.Add(actualCube);
        }
        else
        {
            list.Clear();
        }
    }

    List<TerrainCube> FindConsecutiveSpaces(int amount, int countStart, bool leftToRight)
    {
        List<TerrainCube> terrainCubes = new List<TerrainCube>();

        TerrainCube auxCube = terrain[countStart];
        int index = countStart;

        terrainCubes.Add(auxCube);

        if (leftToRight)
        {
            for (int i = countStart + 1; i < terrain.Length && terrainCubes.Count < amount; i++)
            {
                ProcessTerrainCubes(terrainCubes, index + 1 == i, auxCube, terrain[i]);

                auxCube = terrain[i];
                index = i;
            }
        }
        else
        {
            for (int i = countStart - 1; i >= 0 && terrainCubes.Count < amount; i--)
            {
                ProcessTerrainCubes(terrainCubes, index - 1 == i, auxCube, terrain[i]);

                auxCube = terrain[i];
                index = i;
            }
        }

        return terrainCubes;
    }

    void PositionEdgeColliders(Vector3 position)
    {
        edgeColliders.transform.position = position;
    }

    void Awake()
    {
        winSpaces = new List<GameObject>();
    }

    void Start()
    {
        SetTerrain();
    }
}
