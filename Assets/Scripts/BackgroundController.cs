﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{
    [SerializeField] GameObject[] layers;

    [SerializeField] float speed;
    [SerializeField] float speedMultiplicator = 0.04f;

    public void Move(Vector2 comparisonPoint, Vector2 centreOfScreen)
    {
        Vector2 target = -(comparisonPoint - centreOfScreen);

        for (int i = 0; i < layers.Length; i++)
        {
            layers[i].transform.position = centreOfScreen + target * speed * i * speedMultiplicator;
        }
    }
}
